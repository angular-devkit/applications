# Purpose
The idea is to demonstrate how to write a typical, non-trivial CRUD application using AngularJS. To showcase AngularJS in its most advantageous environment we've set out to write a simplified project management tool supporting teams using the SCRUM methodology. The sample application tries to show best practices when it comes to: folders structure, using modules, testing, communicating with a REST back-end, organizing navigation, addressing security concerns (authentication / authorization).

# Stack
Persistence store: MongoDB hosted on MongoLab
Backend: Node.js
Awesome AngularJS on the client
CSS based on Twitter's bootstrap

# Build
It is a complete project with a build system focused on AngularJS apps and tightly integrated with other tools commonly used in the AngularJS community:
powered by Grunt.js
test written using Jasmine syntax
test are executed by Karma Test Runner (integrated with the Grunt.js build)
build supporting JS, CSS and AngularJS templates minification
Twitter's bootstrap with LESS templates processing integrated into the build
Travis-CI integration

# Configure Server
The server stores its data in a MongoLab database.
Create an account at MongoLab - it's free: [https://mongolab.com/signup/].
Create a database to use for this application: [https://mongolab.com/create]
Grab your API key: [https://mongolab.com/user?username=YOUR_USERNAME_HERE]
My Test Server:["host:"xhttp://146.185.195.10/main/", port: 8888" + "host:"xhttp://146.185.195.10/main/", port: 8282"]
Edit server/config.js to set your MongoLab API Key and the name of the database you created.

# Browser Support
We only regularly test against Chrome 29 and occasionally against Firefox and Internet Explorer. The application should run on most modern browsers that are supported by the AngularJS framework. Obviously, if you chose to base your application on this one, then you should ensure you do your own testing against browsers that you need to support.

# Continuous testing
You can have grunt (karma) continuously watch for file changes and automatically run all the tests on every change, without rebuilding the distribution files. This can make the test run faster when you are doing test driven development and don't need to actually run the application itself.